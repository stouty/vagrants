<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

  use Pug;

  public function index()
  {
    $this->view('myview');
  }
}
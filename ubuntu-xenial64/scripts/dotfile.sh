#!/usr/bin/env bash

# history
alias h=history
alias hg="history | grep"
alias hgi="history | grep -i"

alias v="cd /vagrant"
alias gw="grunt watch"

# chmod commands
alias cx="chmod +x"
alias cw="chmod +w"
alias cr="chmod +r"

# Detect which `ls` flavor is in use
if ls --color > /dev/null 2>&1; then # GNU `ls`
	colorflag="--color"
else # OS X `ls`
	colorflag="-G"
fi

alias lst="ls -lart ${colorflag}"
alias ll="ls -l ${colorflag}"

# Long form no user group, color
alias l="ls -o ${colorflag}"

# List all files colorized in long format, including dot files
#alias la="ls -la ${colorflag}"

# List only directories
alias lsd='ls -l ${colorflag} | grep "^d"'

# Order by last modified, long form no user group, color
alias lt="ls -to ${colorflag}"

# List all except . and ..., color, mark file types, long form no user group, file size
alias la="ls -AFoh ${colorflag}"

# List all except . and ..., color, mark file types, long form no use group, order by last modified, file size
alias lat="ls -AFoth ${colorflag}"
alias h=history
alias hg="history | grep"
alias hgi="history | grep -i"
alias he="cat ${HOME}/.bash_eternal_history"
alias hge="cat ${HOME}/.bash_eternal_history | grep "
alias hgei="cat ${HOME}/.bash_eternal_history | grep -i "

function psg ()
{
    ps -aef | grep -i "[${1:0:1}]${1:1}";
    return 0
}

# Show the top 10 processes.
function top10 {
	# Default columns displayed by "top" on Debian Linux and Mac OS X:
	#         1   2    3  4  5    6   7   8 9    10   11    12
	# Linux:  PID USER PR NI VIRT RES SHR S %CPU %MEM TIME+ COMMAND
	# Darwin: PID COMMAND %CPU TIME #TH #WQ #PORTS #MREGS RPRVT RSHRD RSIZE VPRVT VSIZE PGRP PPID STATE UID FAULTS COW MSGSENT MSGRECV SYSBSD SYSMACH CSW PAGEINS USER
	#         1   2       3    4    5   6   7      8      9     10    11    12    13    14   15   16    17  18     19  20      21      22     23      24  25      26
	if [[ "$OSTYPE" =~ ^darwin ]]; then
		# Mac OS X's top does not calculate the CPU usage when sampling only
		# once. So, sample twice and only keep the output of the second
		# sample. This makes it seem very slow, but them's the breaks.
		local output="$(top -l 2 -n 10 -o CPU -stats PID,USER,CPU,VSIZE,RSIZE,RSHRD,STATE,TIME,COMMAND)";
		output="$(tail -n "$(($(wc -l <<< "$output") / 2))" <<< "$output")";
	else
		# Debian's top (and possibly other Linux versions) does not allow
		# requesting specific fields on the command line, so create a
		# temporary .toprc somewhere and use that. The "fieldscur" below
		# correspond to the "-stats" option for OS X's "top". See above.
		local tmp_dir="$(mktemp -d -t "top10.$$.XXXX")";
		cat > "$tmp_dir/.toprc" <<-EOD
			RCfile for "top with windows"		# shameless braggin'
			Id:a, Mode_altscr=0, Mode_irixps=1, Delay_time=3.000, Curwin=0
			Def	fieldscur=AEKhiOQTWnMbcdfgjplrsuvyzX
				winflags=62777, sortindx=10, maxtasks=0
				summclr=1, msgsclr=1, headclr=3, taskclr=1
		EOD
		local output="$(HOME="$tmp_dir"; top -bn 1)";
		rm -rf "$tmp_dir";
	fi;

	# Output the result, and use "column" to align the process columns a bit
	# better than "top" itself does. This does screw up the command names when
	# they contain a space, e.g. "Chromium Helper" => "Chromium     Helper".
	local pid_start_line="$(egrep -nm1 '^\s*PID' <<< "$output" || echo 65536)";
	pid_start_line="${pid_start_line%:*}";
	head -n $(($pid_start_line - 1)) <<< "$output";
	tail -n +$pid_start_line <<< "$output" | head -n 11 | column -t;
}


# Sort the "du" output and use human-readable units.
function duh {
	du -sk "$@" | sort -n | while read size fname; do
		for unit in KiB MiB GiB TiB PiB EiB ZiB YiB; do
			if [ "$size" -lt 1024 ]; then
				echo -e "${size} ${unit}\t${fname}";
				break;
			fi;
			size=$((size/1024));
		done;
	done;
}

export HH_CONFIG=hicolor

# Keep a reasonably long history.
export HISTSIZE=10000;

# Keep even more history lines inside the file, so we can still look up
# previous commands without needlessly cluttering the current shell's history.
export HISTFILESIZE=32768;

# When executing the same command twice or more in a row, only store it once.
export HISTCONTROL=ignoreboth;

# Show what a given command really is. It is a combination of "type", "file"
# and "ls". Unlike "which", it does not only take $PATH into account. This
# means it works for aliases and hashes, too. (The name "whatis" was taken,
# and I did not want to overwrite "which", hence "wtfis".)
# The return value is the result of "type" for the last command specified.
function wtfis {
	local cmd type i=1 ret=0;
	if [ $# -eq 0 ]; then
		# Use "fc" to get the last command, and use that when no command
		# was given as a parameter to "wtfis".
		set -- $(fc -nl -1);
		while [ $# -gt 0 -a '(' "sudo" = "$1" -o "-" = "${1:0:1}" ')' ]; do
			# Ignore "sudo" and options ("-x" or "--bla").
			shift;
		done;
		# Replace the positional parameter array with the last command name.
		set -- "$1";
	fi;
	for cmd; do
		type="$(type "$cmd")";
		ret=$?;
		if [ $ret -eq 0 ]; then
			# Try to get the physical path. This works for hashes and
			# "normal" binaries.
			local path="$(type -p "$cmd")";
			if [ -z "$path" ]; then
				# Show the output from "type" without ANSI escapes.
				echo "${type//$'\e'/\\033}";

				case "$(command -v "$cmd")" in
					'alias')
						local alias_="$(alias "$cmd")";
						# The output looks like "alias foo='bar'"; so
						# strip everything except the body.
						alias_="${alias_#*\'}";
						alias_="${alias_%\'}";
						# Use "read" to process escapes. E.g. 'test\ it'
						# will # be read as 'test it'. This allows for
						# spaces inside command names.
						read -d ' ' alias_ <<< "$alias_";
						# Recurse and indent the output.
						# TODO: prevent infinite recursion
						wtfis "$alias_" 2>&2 | sed 's/^/  /';
						;;
					'keyword' | 'builtin')
						# Get the one-line description from the built-in
						# help, if available. Note that this does not
						# guarantee anything useful, though. Look at the
						# output for "help set", for instance.
						help "$cmd" 2> /dev/null | {
							local buf line;
							read -r line;
							while read -r line; do
								buf="$buf${line/.  */.} ";
								if [[ "$buf" =~ \.\ $ ]]; then
									echo "$buf";
									break;
								fi;
							done;
						};
						;;
				esac;
			else
				# For physical paths, get some more info.
				# First, get the one-line description from the man page.
				# ("col -b" gets rid of the backspaces used by OS X's man
				# to get a "bold" font.)
				(COLUMNS=10000 man "$(basename "$path")" 2>/dev/null) | col -b | \
				awk '/^NAME$/,/^$/' | {
					local buf line;
					read -r line;
					while read -r line; do
						buf="$buf${line/.  */.} ";
						if [[ "$buf" =~ \.\ $ ]]; then
							echo "$buf";
							buf='';
							break;
						fi;
					done;
					[ -n "$buf" ] && echo "$buf";
				}

				# Get the absolute path for the binary.
				local full_path="$(
					cd "$(dirname "$path")" \
						&& echo "$PWD/$(basename "$path")" \
						|| echo "$path"
				)";

				# Then, combine the output of "type" and "file".
				local fileinfo="$(file "$full_path")";
				echo "${type%$path}${fileinfo}";

				# Finally, show it using "ls" and highlight the path.
				# If the path is a symlink, keep going until we find the
				# final destination. (This assumes there are no circular
				# references.)
				local paths=("$path") target_path="$path";
				while [ -L "$target_path" ]; do
					target_path="$(readlink "$target_path")";
					paths+=("$(
						# Do some relative path resolving for systems
						# without readlink --canonicalize.
						cd "$(dirname "$path")";
						cd "$(dirname "$target_path")";
						echo "$PWD/$(basename "$target_path")"
					)");
				done;
				local ls="$(command ls -fdalF "${paths[@]}")";
				echo "${ls/$path/$'\e[7m'${path}$'\e[27m'}";
			fi;
		fi;

		# Separate the output for all but the last command with blank lines.
		[ $i -lt $# ] && echo;
		let i++;
	done;
	return $ret;
}

# @gf3’s Sexy Bash Prompt, inspired by “Extravagant Zsh Prompt”
# Shamelessly copied from https://github.com/gf3/dotfiles and 
# https://github.com/necolas/dotfiles/blob/master/bash/bash_prompt
# Screenshot: http://i.imgur.com/s0Blh.png
if [[ $COLORTERM = gnome-* && $TERM = xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
    export TERM=gnome-256color
elif infocmp xterm-256color >/dev/null 2>&1; then
    export TERM=xterm-256color
fi

tput sgr 0 0

# Base styles and color palette
# Solarized colors
# https://github.com/altercation/solarized/tree/master/iterm2-colors-solarized
BOLD=$(tput bold)
RESET=$(tput sgr0)
SOLAR_YELLOW=$(tput setaf 136)
SOLAR_ORANGE=$(tput setaf 166)
SOLAR_RED=$(tput setaf 124)
# shellcheck disable=SC2034
SOLAR_MAGENTA=$(tput setaf 125)
# shellcheck disable=SC2034
SOLAR_VIOLET=$(tput setaf 61)
# shellcheck disable=SC2034
SOLAR_BLUE=$(tput setaf 33)
SOLAR_CYAN=$(tput setaf 37)
SOLAR_GREEN=$(tput setaf 64)
SOLAR_WHITE=$(tput setaf 254)

style_user="\[${RESET}${SOLAR_ORANGE}\]"
style_host="\[${RESET}${SOLAR_YELLOW}\]"
style_path="\[${RESET}${SOLAR_GREEN}\]"
style_chars="\[${RESET}${SOLAR_WHITE}\]"
style_branch="${SOLAR_CYAN}"

if [[ "$SSH_TTY" ]]; then
    # connected via ssh
    style_host="\[${BOLD}${SOLAR_RED}\]"
elif [[ "$USER" == "root" ]]; then
    # logged in as root
    style_user="\[${BOLD}${SOLAR_RED}\]"
fi

is_cocoapods_repo() {
    local repo_name
    repo_name=$(git rev-parse --show-toplevel 2> /dev/null)
    if [[ "$repo_name" =~ cocoapods ]]; then
        return 0
    else
        return 1
    fi
}

is_git_repo() {
    $(git rev-parse --is-inside-work-tree &> /dev/null)
}

is_git_dir() {
    $(git rev-parse --is-inside-git-dir 2> /dev/null)
}

get_git_branch() {
    local branch_name

    # Get the short symbolic ref
    branch_name=$(git symbolic-ref --quiet --short HEAD 2> /dev/null) ||
    # If HEAD isn't a symbolic ref, get the short SHA
    branch_name=$(git rev-parse --short HEAD 2> /dev/null) ||
    # Otherwise, just give up
    branch_name="(unknown)"

    printf "%s" "$branch_name"
}

# Git status information
prompt_git() {
    local git_info git_state uc us ut st

    if ! is_git_repo || is_git_dir; then
        return 1
    fi

    if ! is_cocoapods_repo; then
        # ensure index is up to date
        git update-index --really-refresh  -q &>/dev/null
    fi

    git_info=$(get_git_branch)

    # remember git will ignore empty dirs
    # unless they contain a .gitignore file
    # so the prompt should be correct
    # with the caveat that it ignores any new empty folders
    if ! is_cocoapods_repo; then
        # Check for uncommitted changes in the index
        if ! $(git diff --quiet --ignore-submodules --cached); then
            uc="+"
        fi

        # Check for unstaged changes
        if ! $(git diff-files --quiet --ignore-submodules --); then
            us="!"
        fi

        # Check for untracked files
        if [ -n "$(git ls-files --others --exclude-standard)" ]; then
            ut="?"
        fi

        # Check for stashed files
        if $(git rev-parse --verify refs/stash &>/dev/null); then
            st="$"
        fi
    fi

    git_state=$uc$us$ut$st

    # Combine the branch name and state information
    if [[ $git_state ]]; then
        git_info="$git_info[$git_state]"
    fi

    printf "${SOLAR_WHITE} on ${style_branch}${git_info}"
}

#export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND}"   # mem/file sync
# if this is interactive shell, then bind hh to Ctrl-r (for Vi mode check doc)
if [[ $- =~ .*i.* ]]; then bind '"\C-r": "\C-a hh -- \C-j"'; fi

# Make new shells get the history lines from all previous
# shells instead of the default "last window closed" history
PROMPT_COMMAND='history 1 >> ${HOME}/.bash_eternal_history'

#my old version
#PROMPT_COMMAND="history -a;history -c; history -r; $PROMPT_COMMAND;"

#hh version 
export PROMPT_COMMAND="history -a; history -n; ${PROMPT_COMMAND};"

# Set the terminal title to the current working directory
PS1="\[\033]0;\h: \w\007\]"
#PS1="\[\033]0;\W\007\]"
# Build the prompt
PS1+="\n" # Newline
PS1+="${style_user}\u" # Username
PS1+="${style_chars}@" # @
PS1+="${style_host}\h" # Host
PS1+="${style_chars}: " # :
PS1+="${style_path}\w" # Working directory
PS1+="\$(prompt_git)" # Git details
PS1+="\n" # Newline
PS1+="${style_chars}\$ \[${RESET}\]" # $ (and reset color)


var config = {};

config.app = {};
config.app.deployment = "Staging";
config.app.port = 3001;
config.app.diagnosticOverride = true;

config.url = {
  base: process.env.STAGING_BASE_URL || 'https://service.bwfirmware.com/stage'
};

config.db = {};
config.db.name = process.env.DB_STAGING_NAME || 'serviceapp_staging';
config.db.user = process.env.DB_STAGING_USER || 'root';
config.db.password = process.env.DB_STAGING_PASSWORD || 'root';

config.redis = {};
config.redis.prefix = 'staging';

module.exports = config;
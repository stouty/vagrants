var config = {};

config.app = {};
config.app.deployment = "Production";
config.app.port = 3000;

config.url = {
  base: process.env.PRODUCTION_BASE_URL || 'https://service.bwfirmware.com'
};

config.db = {};
config.db.name = process.env.DB_PRODUCTION_NAME || 'serviceapp_production';
config.db.user = process.env.DB_PRODUCTION_USER || 'root';
config.db.password = process.env.DB_PRODUCTION_PASSWORD || 'root';

config.redis = {};
config.redis.prefix = 'production';

config.mail = {};
config.mail.host = '192.168.10.20';
config.mail.port = 25;
config.mail.username = "service-no-reply@bwgroup.com";
config.mail.password = null;
config.mail.useSSL = false;

module.exports = config;
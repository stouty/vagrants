// var config = {};

// config.app = {};
// config.app.name = "Service App API";
// config.app.serversideDiagnostics = true;

// config.url = {};
// config.url.base = 'https://service.bwfirmware.com';
// config.url.createAccount = '/public/create_account';
// config.url.resetPassword = '/public/password_reset';
// config.url.location = '/admin/location';
// config.url.user = '/admin/user';

// config.auth = {};
// config.auth.workFactor = 10;
// config.auth.alwaysPrompt = true;
// config.auth.adminsMaxLockAttempts = 3;
// config.auth.locationsMaxLockAttempts = 50;

// config.groups = {
//   admin: [
//     'canGetUserMe',
//     'canViewAdminScreen',
//     'canManageUsers',
//     'canManageProducts',
//     'canManageCompanies',
//     'canManageLocations',
//     'canManageWorkstations',
//     'canViewServices',
//     'canGetServices',
//     'canGetServiceLevels',
//     'canGetProducts',
//     'canManageAllCompanies',
//     'canManageAllLocations',
//     'canManageAllWorkstations'
//   ],
//   distributorAdmin: [
//     'canGetUserMe',
//     'canViewAdminScreen',
//     'canManageCompanies',
//     'canManageLocations',
//     'canManageWorkstations',
//     'canViewServices',
//     'canGetServices',
//     'canGetServiceLevels',
//     'canGetProducts'
//   ],
//   serviceLocation: [
//     'canGetUserMe',
//     'canGetWorkstationMe',
//     'canCreateWorkstations',
//     'canService',
//     'canGetServices',
//     'canGetProducts',
//     'canGetServiceLevels',
//     'canGetLocationServiceLevels'
//   ]
// };

// config.redis = {};
// config.redis.host = '127.0.0.1';
// config.redis.port = 6379;
// config.redis.password = process.env.REDIS_PASSWORD || 'serviceapp';

// config.jobs = {};
// config.jobs.retries = 5;
// config.jobs.maxConcurrent = 20;
// config.jobs.backoff = { delay: 1000*1000, type:'exponential' }; // = 1000 seconds (delay is in ms)

// config.mail = {};
// config.mail.protocol = 'SMTP';
// config.mail.port = 465;
// config.mail.useSSL = true;
// config.mail.host = 'mail.coderus.com';
// config.mail.username = 'sts@coderus.com';
// config.mail.password = 'FnK80r+tFhTI';
// config.mail.disableSending = false;

// module.exports = config;
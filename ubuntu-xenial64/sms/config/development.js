var config = {};

config.app = {};
config.app.deployment = "Development";
config.app.port = 3002;
config.app.diagnosticOverride = true;

config.auth = {};
config.auth.workFactor = 1;

// config.url = {
//   base: process.env.DEVELOPMENT_BASE_URL || 'https://service.bwfirmware.com/dev'
// };

config.db = {};
config.db.name = process.env.DB_DEVELOPMENT_NAME || 'SMS';
config.db.user = process.env.DB_DEVELOPMENT_USER || 'root';
config.db.password = process.env.DB_DEVELOPMENT_PASSWORD || 'secret';
// config.db.synchronize = true;

// config.redis = {};
// config.redis.prefix = 'development';

// config.mail = {};
// config.mail.host = 'localhost';
// config.mail.port = 1025;
// config.mail.username = 'serviceapp@coderus.com';
// config.mail.password = null;
// config.mail.useSSL = false;

module.exports = config;
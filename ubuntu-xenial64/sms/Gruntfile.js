var glob = require('glob');

/**
 * Created by eddiewoodley on 19/03/2014.
 */
module.exports = function (grunt) {

  // Load all of the grunt plugins
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
  // grunt.loadTasks('./tasks/profiler/tasks');


  // // Hacky way to enable jasmine-only with grunt-jasmine-node
  // require('jasmine-node');
  // require('jasmine-only');

  var config = {

    /*
     * General
     */
    // clean: {
    //   unit: 'reports/unit',
    //   cover: 'reports/coverage',
    //   reports: 'reports',
    //   docs: 'api-docs/generated'
    // },

    // /*
    //  * General
    //  */
    // copy: {
    //   docs: {
    //     src: 'src/**/*.md',
    //     dest: 'api-docs/build/_posts/',
    //     flatten: true,
    //     expand: true,
    //     rename: function (dest, src) {
    //       //need to add a date to the start of each filename
    //       // to enable jekyll to serve them
    //       return dest + "2013-12-24-" + src;
    //     }
    //   }
    // },
    /*
     * Linting
     */
    jshint: {
      options: {
        node: true,

        // Enforcing
        camelcase: true,
        curly: true,
        indent: 2,
        newcap: true,
        unused: true,
        undef: true,
        trailing: true,

        // Relaxing
        laxcomma: true,

        // Globals
        globals: {
          // App
          projectRequire: true,
          model: true,

          // Testing
          jasmine: true,
          describe: true,
          it: true,
          beforeEach: true,
          afterEach: true,
          expect: true,
          waits: true,
          protractor: true,
          browser: true,
          element: true,
          By: true,
          $: true,
          $$: true,
          get: true,
          put: true,
          post: true,
          del: true,
          run: true
        }
      },
      js: ['src/**/*.js']
    },

    /*
     * Tests
     */
    // jasmine_node: {
    //   options: {
    //     projectRoot: 'src/'
    //   },

      /*
       * Unit Tests (.spec.js)
       */
      // unit: {
      //   options: {
      //     jUnit: {
      //       report: true,
      //       savePath: "reports/unit/",
      //       useDotNotation: true,
      //       consolidate: true
      //     }
      //   }
      // },

      /*
       * API E2E (Integration) Tests (.e2e.js)
       */
    //   integration: {
    //     options: {
    //       specNameMatcher: 'e2e',
    //       jUnit: {
    //         report: true,
    //         savePath: "reports/integration/",
    //         useDotNotation: true,
    //         consolidate: true
    //       }
    //     }
    //   }

    // },

    // /*
    //  * Performance Tests
    //  */
    // profiler: {
    //   options: {
    //     filePattern: 'src/**/*.perf.js',
    //     iterations: 1000,
    //     request: {
    //       baseUrl: 'http://localhost:3002',
    //       auth: {
    //         user: "coderusdev+admin@gmail.com",
    //         pass: "passw0rd",
    //         sendImmediately: true
    //       }
    //     },
    //     reports: {
    //       csv: {
    //         outputFile: 'reports/performance.csv'
    //       },
    //       console: {
    //         format: 'milli'
    //       }
    //     }
    //   },
    //   api: {}
    // },

    /*
     * Protractor
     */
    // protractor: {
    //   options: {
    //     configFile: "./protractor/ptor.conf.js" // Default config file
    //   },
    //   grid: {
    //   },
    //   local: {
    //     configFile: "./protractor/ptor-local.conf.js"
    //   }
    // },

    /*
     * Shell
     */
    // shell: {
    //   options: {
    //     stdout: true,
    //     stderr: true,
    //     failOnError: false
    //   },
    //   coverage: {
    //     command: 'NODE_ENV=development istanbul cover -x *.spec.js --dir reports/coverage node_modules/jasmine-node/bin/jasmine-node -- src'
    //   },
    //   generate_docs: {
    //     command: [
    //       //'rvm use 1.9.3', ---- INSTALLED BY DEFAULT ON THE VAGRANT BOX
    //       'jekyll build -s api-docs/build -d api-docs/generated'
    //     ].join('&&')
    //   },
    //   test_report: {
    //     command: 'grunt jasmine_node:unit | sed -r "s/\\x1B\\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]//g" > ./reports/unittests.txt'
    //   },
    //   kill_nodes: {
    //     command: 'killall node'
    //   },
    //   jasmine_node: {
    //     command: function (file) {
    //       return 'NODE_ENV=development; node --debug-brk node_modules/jasmine-node/lib/jasmine-node/cli.js ' + file;
    //     }
    //   }
    // },

    /*
     * Server Restart (nodemon)
     */
    nodemon: {
      dev: {
        script: 'src/server.js',
        options: {
          env: {
            'NODE_ENV': 'development'
          },
          "verbose": true,
          watch: ['src'],
          ext: 'js',
          delay: 0,
          legacyWatch: true
        }
      }
    },

    'node-inspector': {
      dev: {
        options: {
          "hidden": ["(.*/)?node_modules/.*", "(.*/)?protractor/.*", "(.*/)?api-docs/.*", "(.*/)?docs/.*"]
        }
      }
    },

    /*
     * Watch
     */
    concurrent: {
      dev: ['delta', 'nodemon'],
      options: {
        logConcurrentOutput: true
      },
      debug: {
        tasks: ['node-inspector', 'nodemon'],
        options: {
          logConcurrentOutput: true
        }
      }
    },

    delta: {
      js: {
        files: ['src/**/*.js'],
        tasks: ['test']
      },
      md: {
        files: ['src/**/*.md'],
        tasks: ['docs']
      }

    },

    // replace: {
    //   sql_comments: {
    //     src: ['deploy/sql/base.sql'],
    //     dest: 'deploy/sql/base_comments_removed.sql',
    //     replacements: [{
    //       from: / ?COMMENT[=| ]+'.*'(,)?/g,
    //       to: '$1'
    //     }]
    //   }
    // }

  };

  grunt.initConfig(config);

  // grunt.registerTask('server', function () {
  //   var done = this.async();
  //   process.env.NODE_ENV = 'development';
  //   var server = require('./src/proxied-server.js');
  //   var maxWaits = 30;
  //   var i = 0;
  //   var wait = function () {
  //     if (server.listening || i++ > maxWaits) {
  //       done();
  //     } else {
  //       grunt.log.writeln('Waiting for server...');
  //       setTimeout(function () {
  //         wait();
  //       }, 1000);
  //     }
  //   };
  //   wait();
  // });

  // grunt.registerTask('testDebug', function (file) {
  //   if (arguments.length === 0) {
  //     grunt.log.writeln(this.name + ": needs a file name.");
  //   } else {
  //     var testFiles = glob.sync(__dirname + "/src/**/" + file);
  //     if (testFiles.length === 0 || testFiles.length > 1) {
  //       grunt.log.writeln(this.name + ": file not found.");
  //     } else {
  //       var filePath = testFiles[0];

  //       var generateTestDebugTask = function (filePath) {
  //         return {
  //           tasks: ['node-inspector', 'shell:jasmine_node:' + filePath],
  //           options: {
  //             logConcurrentOutput: true
  //           }
  //         };
  //       };

  //       config.concurrent.testDebug = generateTestDebugTask(filePath);
  //       grunt.registerTask('testDebugFile', ['shell:kill_nodes', 'concurrent:testDebug']);
  //       grunt.task.run('testDebugFile');
  //     }
  //   }
  // });

  // grunt.registerTask('cover', ['clean:cover', 'clean:docs', 'jshint', 'shell:coverage']);
  // grunt.registerTask('docs', ['clean:docs', 'clean:cover', 'copy:docs', 'shell:generate_docs']);
  // grunt.registerTask('test', ['clean:unit', 'jshint', 'jasmine_node:unit']);

  grunt.renameTask('watch', 'delta');
  grunt.registerTask('watch', ['test', 'concurrent:dev']);
  grunt.registerTask('debug', ['concurrent:debug']);

  grunt.registerTask('sql', ['replace:sql_comments']);

  grunt.registerTask('integrate', [
    'clean:reports',
    'jshint',
    'server',
    'jasmine_node:integration'
  ]);

  grunt.registerTask('integrate_ui', [
    'clean:reports',
    'jshint',
    'server',
    'protractor:grid'
  ]);

};

var _ = require('lodash');

module.exports.handler = function (err, req, res, next) { // jshint ignore:line
  if (err) {
    if (_.isString(err)) {
      res.status(500).send({ type: "UnexpectedError", description: err, extra: {} });
    } else if (_.isObject(err)) {
      var type = err.message || "UnexpectedError";
      var extra = err.extra || {};
      var description = err.description || false;
      var code = err.status || -1;

      if (code < 0) {
        if (type === "UnexpectedError") {
          code = 500;
          description = description || "An Unexpected Error occurred.";
        } else if (type.search(/(badrequest|invalid|ServiceLevelError|BadFingerprint)/i) != -1) {
          code = 400;
          description = description || "One or more parameters were invalid.";
        } else if (type.search(/unauthorized/i) != -1) {
          code = 401;
          description = description || "Authentication credentials were missing or incorrect.";
        } else if (type.search(/locked/i) != -1) {
          code = 401;
          description = description || "Your account has been locked.";
        } else if (type.search(/inactive/i) != -1) {
          code = 401;
          description = description || "Your account has been deactivated.";
        } else if (type.search(/forbidden/i) != -1) {
          code = 403;
          description = description || "Not allowed to perform that request.";
        } else if (type.search(/notfound/i) != -1) {
          code = 404;
          description = description || "Resource Not Found.";
        } else if (type.search(/duplicate/i) != -1 || type.search(/conflict/i) != -1) {
          code = 409;
          description = description || "Duplicate entry.";
        } else if (type.search(/decrypt/i) != -1) {
          code = 400;
          description = description || "Could not decrypt.";
        } else if (type.search(/decrypt/i) != -1) {
          code = 400;
          description = description || "Could not decrypt.";
        }
        else {
          code = 500;
          description = description || "An Unexpected Error occurred.";
        }
      }


      res.status(code).send({
        type: type,
        description: description,
        extra: extra
      });

    }
  }
};
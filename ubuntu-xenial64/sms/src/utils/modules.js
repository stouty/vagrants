/**
 * Created by eddiewoodley on 24/04/2014.
 */
var glob = require('glob');

module.exports.load = function(app){
  var loadModule = function(file){
    var m = require(file);
    if(m.init){
      m.init(app);
    }
  };

  glob.sync(__dirname + '/../common/**/*.module.js').forEach(loadModule);

  glob.sync(__dirname + '/../app/**/*.module.js').forEach(loadModule);
};
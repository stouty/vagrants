/**
 * Created by eddiewoodley on 24/04/2014.
 */
var glob = require('glob');

var commonModels = glob.sync(__dirname + "/../common/**/*.model.js");
var appModels = glob.sync(__dirname + "/../app/**/*.model.js");
var commonMappingModels = glob.sync(__dirname + "/../common/**/*.model.mapping.js");
var appMappingModels = glob.sync(__dirname + "/../app/**/*.model.mapping.js");

module.exports.load = function (sequelize) {
  var models = {
    sequelize: sequelize
  };

  var imported = [];

  var loadModel = function (file) {
    var model = sequelize.import(file);
    models[model.name] = model;
    imported.push(file);
  };

  var loadModelRelationships = function (file) {
    var model = require(file);
    if (model.init) {
      model.init(models);
    }
  };

  commonModels.forEach(loadModel);
  appModels.forEach(loadModel);
  commonMappingModels.forEach(loadModel);
  appMappingModels.forEach(loadModel);

  imported.forEach(loadModelRelationships);

  return models;
};
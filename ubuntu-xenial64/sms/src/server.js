var express = require('express')
  , Sequelize = require('sequelize')
  , bodyParser = require('body-parser')
  , CONFIG = require('config')
  , winston = require('winston');

require('monitor').start();

// Add the global for projectRequire.
// This allows modules to easily include parts of other modules in a consistent manner.
global.projectRequire = function (path) {
  path = __dirname + '/' + path;
  return require(path);
};

// Configure the logger
winston.add(winston.transports.File, { filename: __dirname + '/../logs/server.log' });

// Connect sequelize
var sequelize = new Sequelize(CONFIG.db.name, CONFIG.db.user, CONFIG.db.password, {logging: false, host: 'localhost',
dialect: 'mysql', define: {
    underscored: false,
    freezeTableName: true,
    charset: 'utf8',
    dialectOptions: {
      collate: 'utf8_general_ci'
    },
    timestamps: false
  },
});

// Express Server Setup
var app = express();
app.set('trust proxy', true);

app.use(bodyParser.urlencoded({
    extended: true
  }));
app.use(bodyParser.json());

// Load models
var models = require('./utils/models.js').load(sequelize);
app.set('models', models);

// Load modules
require('./utils/modules.js').load(app);

// Error handling
app.use(function (err, req, res, next) {
  winston.error(err.message);
  next(err);
});
app.use(require('./utils/errorHandler.js').handler);

var serverStart = function () {
  app.listen(CONFIG.app.port, function (err) {
    if (err) {
      winston.error('Error starting server:');
      winston.error(err);
    } else {
      winston.info(CONFIG.app.deployment + ' server started.');
      module.exports.listening = true;
    }
  });
};

serverStart();

module.exports = app;
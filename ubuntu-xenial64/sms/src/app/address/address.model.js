module.exports = function (sequelize, DataTypes) {
    var schema = {
    idAddress: {
        type: DataTypes.INTEGER,
        comment: "The address id.",
        primaryKey: true
      },
      country: {
        type: DataTypes.STRING,
        comment: "The country."
      },
      address: {
        type: DataTypes.STRING,
        comment: "The requested URL."
      }
    };
  
    return sequelize.define('Address', schema, {
        comment: "Contains logs of all requests made to the server. "
    });
  };
